package ru.t1.ktubaltseva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.command.project.*;
import ru.t1.ktubaltseva.tm.command.system.*;
import ru.t1.ktubaltseva.tm.command.task.*;
import ru.t1.ktubaltseva.tm.command.user.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.CommandRepository;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;
import ru.t1.ktubaltseva.tm.repository.UserRepository;
import ru.t1.ktubaltseva.tm.service.*;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectDisplayByIdCommand());
        registry(new ProjectDisplayByIndexCommand());
        registry(new ProjectDisplayListCommand());
        registry(new ProjectDisplayListFullInfoCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskDisplayByIdCommand());
        registry(new TaskDisplayByIndexCommand());
        registry(new TaskDisplayByProjectIdCommand());
        registry(new TaskDisplayListCommand());
        registry(new TaskDisplayListFullInfoCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserDisplayProfileCommand());
        registry(new UserLockCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserRemoveCommand());
        registry(new UserUnlockCommand());
        registry(new UserUpdateProfileCommand());
    }

    private void initDemoData() throws AbstractException, NoSuchAlgorithmException {

        @NotNull final User user1 = userService.create("USER1", "USER1");
        @NotNull final User user2 = userService.create("USER2", "USER2");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

        projectService.add(user1.getId(), new Project("pn2", "pd2", Status.IN_PROGRESS));
        projectService.add(user1.getId(), new Project("pn5", "pd5", Status.NOT_STARTED));
        projectService.add(user2.getId(), new Project("pn3", "pd3", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("pn4", "pd4", Status.COMPLETED));

        taskService.add(user1.getId(), new Task("tn2", "td2", Status.IN_PROGRESS));
        taskService.add(user1.getId(), new Task("tn5", "td5", Status.NOT_STARTED));
        taskService.add(user2.getId(), new Task("tn3", "td3", Status.IN_PROGRESS));
        taskService.add(admin.getId(), new Task("tn4", "td4", Status.COMPLETED));
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private boolean processArgument(@Nullable final String[] args) throws ArgumentNotSupportedException, CommandNotSupportedException {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) throws ArgumentNotSupportedException, CommandNotSupportedException {
        if (argument == null) throw new ArgumentNotSupportedException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
    }

    private void processCommand(@Nullable final String command) throws AbstractException, NoSuchAlgorithmException {
        if (command == null) throw new CommandNotSupportedException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        getAuthService().checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void run(@Nullable final String[] args) throws AbstractException, NoSuchAlgorithmException {
        if (processArgument(args)) System.exit(0);

        initDemoData();
        initLogger();

        processCommands();
    }

}
