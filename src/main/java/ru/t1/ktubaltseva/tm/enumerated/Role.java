package ru.t1.ktubaltseva.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Admin user");

    private @NotNull
    final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    @Override
    public String toString() {
        return getDisplayName();
    }
}
