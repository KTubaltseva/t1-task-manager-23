package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.AbstractEntityNotFoundException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    M add(
            @Nullable String userId,
            @Nullable M model
    ) throws AbstractEntityNotFoundException, AuthRequiredException;

    void clear(@Nullable String userId) throws AuthRequiredException;

    @NotNull
    List<M> findAll(@Nullable String userId) throws AuthRequiredException;

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Comparator<M> comparator
    ) throws AuthRequiredException;

    @NotNull
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    @NotNull
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws AbstractException;

    void removeAll(@Nullable String userId) throws AuthRequiredException;

    @NotNull
    M removeOne(
            @Nullable String userId,
            @Nullable M model
    ) throws AbstractException;

    @NotNull
    M removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    @NotNull
    M removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws AbstractException;

}
