package ru.t1.ktubaltseva.tm.util;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "8800555";

    @NotNull
    Integer ITERATION = 3535;

    @NotNull
    static String salt(@NotNull final String value) throws NoSuchAlgorithmException {
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + value + SECRET);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) throws NoSuchAlgorithmException {
        @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
        @NotNull final byte[] array = md.digest(value.getBytes());
        @NotNull final StringBuffer sb = new StringBuffer();
        for (byte b : array) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
        }
        return sb.toString();
    }

}
