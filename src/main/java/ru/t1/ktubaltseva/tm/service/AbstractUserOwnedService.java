package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.api.service.IUserOwnedService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R modelRepository) {
        super(modelRepository);
    }

    @NotNull
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws EntityNotFoundException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        modelRepository.add(userId, model);
        return model;
    }

    @Override
    public void clear(@Nullable final String userId) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        modelRepository.clear(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        return modelRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (comparator == null) return findAll(userId);
        return modelRepository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = modelRepository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model = modelRepository.findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public void removeAll(@Nullable String userId) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        modelRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable final M modelRemoved = modelRepository.removeOne(userId, model);
        if (modelRemoved == null) throw new EntityNotFoundException();
        return modelRemoved;
    }

    @NotNull
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M modelRemoved = modelRepository.removeById(userId, id);
        if (modelRemoved == null) throw new EntityNotFoundException();
        return modelRemoved;
    }

    @NotNull
    @Override
    public M removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M modelRemoved = modelRepository.removeByIndex(userId, index);
        if (modelRemoved == null) throw new EntityNotFoundException();
        return modelRemoved;
    }

}
