package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-complete-by-index";

    @NotNull
    private final String DESC = "Complete project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        getProjectService().changeProjectStatusByIndex(getUserId(), index, Status.COMPLETED);
    }

}
